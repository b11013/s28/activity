//create users
db.users.insertMany([
    {
            firstName : "Serj",
            lastName : "Tankian",
            email: "serj.tankian@mail.com",
            password:"iamvocals",
            isAdmin:false
            
    },
    {
            firstName : "Daron",
            lastName : "Malakian",
            email: "daron.malakian@mail.com",
            password:"iamleadguitar",
            isAdmin:false
    },
    {
            firstName : "Shavo",
            lastName : "Odadjian",
            email: "shavo.odadjian@mail.com",
            password:"iambass",
            isAdmin:false
    },
    {
            firstName : "John",
            lastName : "Dolmayan",
            email: "john.dolmayan@mail.com",
            password:"iamdrums",
            isAdmin:false
    },
    {
            firstName : "Arto",
            lastName : "Tunçboyacıyan",
            email: "arto.tunçboyacıyan@mail.com",
            password:"iampercussion",
            isAdmin:false
    }

])

//create courses
db.courses.insertMany([
    {
            name : "HTML",
            price : 600,
            isActive: false
    },
    {
                name : "PHP",
                price : 1000,
                isActive: false         
    },
    {
                name : "Java",
                price : 1500,
                isActive: false           
    }

])

//read non-admin
db.users.find({isAdmin: false})

//update first user as admin
db.users.updateOne({},{ $set: {isAdmin: true}})

//update one of the courses as active
db.courses.updateOne({name: 'PHP'},{ $set: {isActive: true}})

//delete all inactive courses
db.courses.deleteMany({isActive: false})
